#include "config.h"

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <stdbool.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>

#ifdef HAVE_SYS_SYSCALL_H
#include <sys/syscall.h>
#endif
#ifdef HAVE_SYSCALL_H
#include <syscall.h>
#endif

#include "uwrap_fake_socket_wrapper.h"

/* simulate socket_wrapper hooks */
bool socket_wrapper_syscall_valid(long int sysno)
{
	if (sysno == __FAKE_SOCKET_WRAPPER_SYSCALL_NO) {
		return true;
	}

	return false;
}

long int socket_wrapper_syscall_va(long int sysno, va_list va)
{
	(void) va; /* unused */

	if (sysno == __FAKE_SOCKET_WRAPPER_SYSCALL_NO) {
		errno = 0;
		return __FAKE_SOCKET_WRAPPER_SYSCALL_RC;
	}

	errno = ENOSYS;
	return -1;
}
